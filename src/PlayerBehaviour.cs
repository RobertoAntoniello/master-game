﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerBehaviour : MonoBehaviour
{
    public float RunningSpeed, RunningAccel, JumpStrength;
    public Transform playerModel;
    private Rigidbody2D _rb;
    private static Collider2D[] grounds = null;
    private Animator _animator;

    public GameLogic gameLogic;
    public LightAttack lightAttack;
    public HeavyAttack heavyAttack;
    private float FireDelay = 0f;   //max 0.2f
    public Transform HandTransform;

    public string PlayerPrefix = "P1_";

    private static Collider2D[] BuildGroundLists()
    {
        List<Collider2D> ground_list = new List<Collider2D>();
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Ground"))
        {
            ground_list.Add(g.GetComponent<Collider2D>());
        }
        return ground_list.ToArray();
    }

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        grounds = grounds ?? BuildGroundLists();
        FireDelay = Time.fixedTime;
    }
    void FixedUpdate()
    {
        //Tramite questo foreach viene verificato se il personaggio sta toccando o meno il terreno	
        bool isGrounded = false;
        foreach (Collider2D c in grounds)
        {
            if (_rb.IsTouching(c))
            {
                isGrounded = true;
                break;
            }

        }
        //rilevo il movimento dell'analogico, in seguito verifico se far partire l'animazione di movimento o se restare fermo	
        float x_move = Input.GetAxis(PlayerPrefix + "Horizontal");
        if (Mathf.Abs(x_move) > 0.1f)
        {
            _animator.SetTrigger("Move");
            playerModel.localScale = Vector3.right * Mathf.Sign(x_move) + new Vector3(0, 1, 1);
            bool b_button = Input.GetButtonDown(PlayerPrefix + "B");
            if (b_button)
            {
                float direction = Mathf.Sign(x_move);
                _rb.AddForce(Vector2.right * direction * RunningAccel, ForceMode2D.Impulse);
            }
        }
        else
        {
            _animator.SetTrigger("Stop");
        }
        //controllo per la velocità massima sul personaggio
        _rb.AddForce(Vector2.right * x_move * Mathf.Lerp(RunningAccel, 0, Mathf.Abs(_rb.velocity.x) / RunningSpeed) * Time.fixedDeltaTime * (isGrounded ? 1 : 0.1f), ForceMode2D.Impulse);

        //Controllo sulla pressione del pulsante 'a' per saltare
        bool a_button = Input.GetButtonDown(PlayerPrefix + "A");
        if (a_button && isGrounded)
        {
            Debug.Log("Pressed A");
            _rb.AddForce(new Vector2(0, JumpStrength) * Time.fixedDeltaTime, ForceMode2D.Impulse);
        }
        //controllo sulla pressione del pulsante 'x' per attaccare 
        bool x_button = Input.GetButtonDown(PlayerPrefix + "X");

        if (x_button && lightAttack.IsReady)
        {
                if (lightAttack.Shoot())
                {
                    gameLogic.ModifyThreshold(lightAttack.Damage);
                }
                _animator.SetTrigger("Attack");
        }
	//controllo del pulsante "y" per l'attacco speciale
        bool y_button = Input.GetButtonDown(PlayerPrefix + "Y");
        if (y_button && heavyAttack.IsReady)
        {
                if (heavyAttack.Shoot())
                {
                    gameLogic.ModifyThreshold(heavyAttack.Damage);
                }
                _animator.SetTrigger("Attack");
        }




        //controllo attacco pesante


    }
}
