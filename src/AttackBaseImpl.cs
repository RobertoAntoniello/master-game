﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AttackBaseImpl : MonoBehaviour {
    public int Damage;
    public float Delay;
    public AudioSource SFX;
    public float ElapsedTime;
    public bool IsReady
    {
        get
        {
            return (ElapsedTime >= Delay);
        }
    }

    public Slider SkillReadiness;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        ElapsedTime += Time.deltaTime;
        SkillReadiness.value = Mathf.Clamp01(ElapsedTime / Delay);
	}

    protected void AddPushback(Rigidbody2D rb)
    {
        Vector2 impactDir = transform.position - rb.transform.position;
        rb.AddForce(impactDir * Mathf.Abs(Damage) + Physics2D.gravity * (-10) * Time.fixedDeltaTime, ForceMode2D.Impulse);
    }
}
