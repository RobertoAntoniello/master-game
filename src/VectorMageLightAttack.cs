﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorMageLightAttack : LightAttack
{
    public float AttackRadius;
    public ParticleSystem MageBlastPunch;
    public override bool Shoot()
    {
        SFX.Play();
        ElapsedTime = 0;
        MageBlastPunch.Play();
        foreach (Collider2D C in Physics2D.OverlapCircleAll(transform.position, AttackRadius))
        {
            if (C.CompareTag("Player"))
            {
                //AddPushback(C.attachedRigidbody);
                return true;
            }
        }
        return false;
    }
    
    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, AttackRadius);
    }
}
