﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Questo è un manager di Input da integrare con l'Input Manager di Unity
/// 
/// Gli Input che trovate tra virgolette devono essere aggiunte al vostro progetto
/// </summary>

public static class InputManager {

    //-- Axis
    public static float MainHorizontal()
    {
        float r = 0.0f;
        r += Input.GetAxis("J_MainHorizontal");
        r += Input.GetAxis("K_MainHorizontal");
        return Mathf.Clamp(r, -1, 1);
    }

    public static float MainVertical()
    {
        float r = 0.0f;
        r -= Input.GetAxis("J_MainVertical");
        r += Input.GetAxis("K_MainVertical");
        return Mathf.Clamp(r, -1, 1);
    }

    public static float SecondaryHorizontal()
    {
        float r = 0.0f;
        r += Input.GetAxis("Mouse X");
        r += Input.GetAxis("J_RightX");
        return Mathf.Clamp(r, -1, 1);
    }

    public static float SecondaryVertical()
    {
        float r = 0.0f;
        r += Input.GetAxis("Mouse Y");
        r += Input.GetAxis("J_RightY");
        return Mathf.Clamp(r, -1, 1);
    }

    public static float Handbrake()
    {
        return Input.GetAxis("A_Button");
    }

    // -- Buttons
	public static bool AButton()
    {
        return Input.GetButtonDown("A_Button");
    }

    public static bool BButton()
    {
        return Input.GetButtonDown("B_Button");
    }

    public static bool XButton()
    {
        return Input.GetButtonDown("X_Button");
    }

    public static bool YButton()
    {
        return Input.GetButtonDown("Y_Button");
    }
}
