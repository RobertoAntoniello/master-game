﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelKnightHeavyAttack : HeavyAttack {
    public float AttackRadius;
    public ParticleSystem KnightKnifeBlast;
    public override bool Shoot()
    {
        ElapsedTime = 0;
        KnightKnifeBlast.Play();
        SFX.Play();
        foreach (Collider2D C in Physics2D.OverlapCircleAll(transform.position, AttackRadius))
        {
            if (C.CompareTag("Player") && !C.name.Equals("Pixel Knight"))
            {
                AddPushback(C.attachedRigidbody);
                return true;
            }
        }
        return false;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, AttackRadius);
    }
}
