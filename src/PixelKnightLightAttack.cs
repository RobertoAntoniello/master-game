﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelKnightLightAttack : LightAttack {
    public float AttackRadius;
    public ParticleSystem SwordSlash;
    public override bool Shoot()
    {
        ElapsedTime = 0;
        SwordSlash.Play();
        SFX.Play();
        foreach (Collider2D C in Physics2D.OverlapCircleAll(transform.position, AttackRadius))
        {
            if (C.CompareTag("Player"))
            {
                AddPushback(C.attachedRigidbody);
                return true;
            }
        }
        return false;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, AttackRadius);
    }
}
