﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimator : MonoBehaviour {
    public Sprite[] Sprites;
    public SpriteRenderer Renderer;
    public int SpriteIndex = 0;
    public float SpriteDelay;
	// Use this for initialization
	void Start () {
        InvokeRepeating("UpdateSprite", 0, SpriteDelay);
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    void UpdateSprite()
    {
        Renderer.sprite = Sprites[SpriteIndex];
        SpriteIndex = (SpriteIndex + 1) % Sprites.Length;
    }
}
