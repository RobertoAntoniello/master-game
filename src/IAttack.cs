﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttack {
    //return true if it hits an enemy
    bool Shoot();

}
