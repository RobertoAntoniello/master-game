﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	// Proprietà Threshold, gestisce il cambio in real time della percentuale di sfondo
    public float Threshold
    {
        set
        {
            value = Mathf.Clamp01(value);
            PixelCamera.transform.position = Vector3.right * SceneWidth * value - Vector3.forward * 10;
            PixelCamera.rect = Rect.MinMaxRect(value, 0, 1, 1);
        }
    }
    public Camera VectorCamera, PixelCamera;
    public float SceneWidth = 8f;
	
	// Update is called once per frame
	void Update () {
        
	}
}
