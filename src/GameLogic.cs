﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    public CameraController cameraCTRL;
    public int Threshold;
    public enum GameState { START, RUNNING, OVER };
    public GameState currentState;

    public PlayerBehaviour Vector, Knight;

    public GameObject MatchUI;

    //Questa funzione si assicura che la partita finisca in determinate condizioni.
    void Update()
    {
        switch (currentState)
        {
            case GameState.START:
                break;
            case GameState.RUNNING:
                if (Threshold <= 0 || Threshold >= 100)
                {
                    currentState = GameState.OVER;
                    if (Threshold <= 0)
                    {
                        Vector.gameObject.SetActive(false);
                    }
                    else
                    {
                        Knight.gameObject.SetActive(false);
                    }
                    Threshold = 50;
                    Invoke("ResetGame", 10f);
                    MatchOver.SetActive(true);
                    MatchUI.SetActive(false);
                }
                break;
            case GameState.OVER:
                break;
        }
    }
    //Funzione che si occupa della modifica del background a seconda del danno inflitto
    public void ModifyThreshold(int Delta)
    {
        Threshold += Delta;
        cameraCTRL.Threshold = Threshold / 100f;
    }

    public GameObject GameMenu;
    public GameObject MatchOver;
	//tramite ResetGame, viene tolto il match over, viene fatto vedere il menù iniziale e tolti i due pg dal campo.	
    public void ResetGame()
    {
        currentState = GameState.START;
        Vector.gameObject.SetActive(false);
        Knight.gameObject.SetActive(false);
        GameMenu.gameObject.SetActive(true);
        MatchOver.SetActive(false);
        cameraCTRL.Threshold = 0.5f;
    }
	//Tramite StartGame il menù viene disabilitato e i due personaggi fatti spawnare sul campo
    public void StartGame()
    {
        currentState = GameState.RUNNING;
        Vector.gameObject.SetActive(true);
        Knight.gameObject.SetActive(true);
        GameMenu.gameObject.SetActive(false);
        MatchUI.SetActive(true);
    }
	//chiude l'applicazione
    public void Quit()
    {
        Application.Quit();
    }
}
