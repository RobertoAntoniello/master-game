﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorMageHeavyAttack : HeavyAttack {
    public ParticleSystem MageHeavyBolt;
    public float MaxRange;
    // Use this for initialization
    public override bool Shoot()
    {
        MageHeavyBolt.Play();
        SFX.Play();
        ElapsedTime = 0;
        foreach (Collider2D C in Physics2D.OverlapBoxAll(transform.position + new Vector3(transform.parent.localScale.x, 0) * MaxRange/2f, new Vector2(MaxRange, 1), 0))
        {
            if (C.CompareTag("Player"))
            {
                AddPushback(C.attachedRigidbody);
                return true;
            }
        }
        return false;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position + transform.right * MaxRange / 2f, new Vector3(MaxRange, 1, 0));
    }
}
